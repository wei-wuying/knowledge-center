# JavaEE (Java2E / JavaWeb)

## 基础概念
### 规范
* sun公司规定了13种javaWeb开发需要遵循的规范，其中Servlet和JSP是核心规范
### 开发过程
* Web前端 -> 业务处理层(Servlet) -> JDBC -> 数据库
### 静态资源和动态资源
#### 静态资源
- 指的是在网站中提供给用户展示的资源是一成不变的，也就是所有人在不同时期或使用的不同用户，看到的内容都是一样的
#### 动态资源
- 指的是在网站中提供给用户展示的资源是由程序产生的，在不同时间或者使用不同账号用户，看到的内容不同
### 广域网和局域网
#### 局域网
- 知道是在一定范围内才可以访问的网络，除了这个范围就不能使用这个网络
#### 广域网 - 万维网
- 也就是互联网，最大的网
### 系统架构
#### 基础结构
- C/S : 客户端/服务器 |(Client)/(Server)
- B/S : 浏览器/服务器 |(Browser)/(Server)
#### 部属方式
- 一体式开发
- 分布式开发
- 微服务开发
### 服务器
* 概念：值得就是特殊的计算机，也就是安装了服务器软件的电脑
  - 如安装了mysql就是安装了mysql服务器
  - 安装了tomcat就是安装了tomcat服务器
### Web服务器
#### 概念
- 本质上就是一个软件，用于接收和处理用户的请求，并做出相应的响应
#### 常见的web服务器
- WebLogic ： Oracle旗下的，支持所有JavaEE规范，但是收费
- JBoss ： JBoss旗下的，一样支持所有JavaEE规范，但收费
- WebSphere ： IBM公司的大型收费web服务器，支持全部JavaEE规范
- Tomcat ： Apache公司，轻量级服务器，仅支持Servlet / JSP，但是免费
## Tomcat
### 目录结构
```text
- bin 脚本目录
    + startup.bat - 启动脚本
    + shutdown.bat - 停止脚本
- conf 配置文件目录 (config / configuration)
    + serve.xml - 核心配置文件
    + tomcat-users.xml - 用户权限配置文件
    + web.xml - 所有web项目默认配置文件
- lib 依赖库，tomcat和web项目中所需要使用到的jar包
- logs 日志文件
    + localhost_access_log.*.txt tomcat - 记录用户访问信息，*表示时间
        如 localhost_access_log.2022-01-10.txt
- temp 临时文件目录，文件夹内容可以任意删除
- webapps 默认情况下发布web项目所存放的目录
- work tomcat处理JSP的工作目录
```
### 配置
```text
  - 出现问题:启动后黑窗口一闪而过
  	+ 原因: 没有正确配置环境变量
  	+ 解决方式:配置JAVA_HOME环境变量
  	  = 变量名: JAVA_HOME
  	  = 变量值: C:\Program Files\Java\jdk1.8.0_321
  	  = 配置path路径: %JAVA_HOME%\bin
  - 启动报错
  	+ 报错: Address ...
  - 配置Tomcat环境变量
  	+ 变量名:JAVA_HOME: JDK的bin目录
  	+ 变量名:CATALINA_HOME
  	+ 变量值:D:\xxx\apache-tomcat-8.5.76
  	+ 配置path路径: %CATALINA_HOME%\bin
  - 将项目部署到Tomcat服务器的方式:
    + 直接将项目放到webapps目录下即可
       /form:项目的访问路径-->虚拟目录
       简化部署:将项目打成一个war包,再将war包放置到webapps目录下。
       war包会自动解压缩
      
    + 配置conf/server.xml文件
       在<Host>标签体中配置
        <Context docBase="D:\hello" path="/hehe" />
          * docBase:项目存放的路径
          * path：虚拟目录
    
    - 在conf\Catalina\localhost创建任意名称的xml文件。
      + 在xml文件中编写: <Context docBase="D:\hello" />
          * 虚拟目录：xml文件的名称
```
## JavaWeb 工程
- JavaWeb的目录结构，也就是正常写代码我们能看到的部分
```text
javaweb_day01  -> 工程名
  |- src: 存放java代码
  |- web: web资源(静态资源)
    |-静态资源(html,jsp)
    |-WEB-INF:(用户不可直接访问)
      |- lib: 存放第三方jar包
      |- web.xml (当前web项目的核心配置文件)

  |- 普通文件夹(资源) -> 部署在web应用下是不识别的资源
```
- tomcat的web应用目录结构
```text
javaweb_day01_war_exploded -> web应用名
  |- WEB-INF
    |- classes: java代码编译后的字节码文件
    |- web.xml
    |- lib: 类库

  |- 静态资源
    html、js、css...
```
## Servlet
* 全称： Server Applet - 服务器小程序
#### 生命周期
* Servlet对象只会创建一次,销毁一次,且在使用中Servlet也仅仅是一个对象，所以Servlet是一个单例(单例模式的单例)
- init - 初始化
- service - 核心方法
- 
#### 方法
- init() - 初始化方法，创建Servlet对象就进行初始化操作，**只跑一次**
- service - 核心的业务方法
- destroy - 销毁方法，只在服务器关闭的时候执行，且只执行一次
#### 执行原理
- 当服务器接收得到浏览器的请求后,会解析URL路径,获取访问Servlet的资源路径.
- 查找web.xml,查看是否有对应的<url-pattern>
- 如果有,则有对应的<servlet-name>,根据servlet的命名找到所对应java类<servlet-class>
- tomcat会将字节码文件加载到内存中,将其创建对象
- 调用对象中的service方法
### 使用步骤
- 定义JavaEE规范
- 定义一个类去实现Servlet接口
- 实现接口中的抽象方法
- 配置Servlet
```dtd
    <!--访问ServletDemo类-->
    <servlet>
        <!--给Servlet定义名称: 随意 -->
        <servlet-name>hello</servlet-name>
        <!--识别Servlet的路径: 全限定类名 -->
        <servlet-class>web.servlet.ServletDemo</servlet-class>
    </servlet>

    <!--映射地址: ServletDemo类对应的资源访问方式 -->
    <servlet-mapping>
        <!--必须和上面的servlet-name一致 -->
        <servlet-name>hello</servlet-name>
        <!--访问的路径: 随意-->
        <url-pattern>/hi</url-pattern>
    </servlet-mapping>
```
- 启动服务器，并且访问Servlet
### 请求转发 - (forward)
- 概念：在服务器内部进行资源跳转的方式
- request 域对象 - 对应request的生命周期
  > 创建：也就是接收请求
  > 销毁：也就是一次请求的结束
  > 范围：很明显，是一次请求的过程
- 作用：在转发时进行数据的传递
- 使用场景：通常Servlet转发到jsp时使用
- **注**：只能转发到当前服务器，而**不能**转发到另一个服务器中
### 响应
#### 功能
- 设置响应的数据 
- 重定向 
- 响应体(字符数据 / 字节数据)
#### 设置响应的数据
- 响应行: 协议/版本 + 响应状态码 + 状态码描述
  - 响应码：服务器给客户端/浏览器传递本次响应的状态
  > 1xx: 信息  
  >	2xx:  
  &nbsp;&nbsp;&nbsp;&nbsp; 200: 表示执行成功  
  >	3xx:  
  &nbsp;&nbsp;&nbsp;&nbsp; 302: 重定向  
  &nbsp;&nbsp;&nbsp;&nbsp; 304: 访问缓存  
  >	4xx:  
  &nbsp;&nbsp;&nbsp;&nbsp; 400: 请求错误,常见请求参数有问题  
  &nbsp;&nbsp;&nbsp;&nbsp; 404: 资源未找到,路径错误  
  &nbsp;&nbsp;&nbsp;&nbsp; 405: 请求的方式不支持  
  >	5xx:  
  &nbsp;&nbsp;&nbsp;&nbsp; 500: 服务器内部出现异常,代码错误  
- 响应头:以map的形式进行存储 
- 响应体:最终响应到浏览器的界面
#### 重定向
- 资源跳转的一种方式
#### 重定向 与 转发
- 请求转发(forward)
    - 特点:
  > - 地址栏不会发生变化  
  > - 只能访问当前服务器下的资源(Servlet)  
  > - 转发是一次请求(涉及到两个Servlet,整个过程1次请求,1次响应)  
  > - 可以使用request域对象来共享数据  

    - 使用场景:
      一个Servlet事情做到一般,需要转发到另外一个Servlet来结合完成.

- 重定向(redirect)
  	- 特点:
  > - 地址栏会发生变化
  > - 重定向可以访问其他站点(服务器)的资源
  > - 重定向是两次请求(涉及两个Servlet,这个过程中2次请求,2次响应)
  > - 不能使用response完成共享数据

    - 使用场景:
      第一个资源已经完成了一件事情,需要做下一件事情, 如注册完成后需要马上跳转到首页


### url配置
#### web.xml配置
**简单理解就是xml中所定义的匹配只是给访问自己的加个约束**，类似正则，如*.do，那么在url中写任意.do都会访问到*.do
- 完全匹配：只能通过**具体路径**来访问所对应的Servlet
- 目录匹配：/开头 + 通配符(*)
- 后缀匹配：字面意思，通过后缀找资源
- 特殊匹配：/ 所有资源，这是默认的配置，又称之为缺省配置，一般不建议使用
#### 注解配置
```text
注解类:WebServlet
    public @interface WebServlet {

    //指的是Servlet的名称 对应标签:<serlvet-name>
    String name() default "";

    //用于映射url,是简写形式,等同于urlPatterns的使用
    String[] value() default {};

    //用于映射url,对应标签: <ur-pattern>
    String[] urlPatterns() default {};

    //设置服务器开启Servlet
    int loadOnStartup() default -1;

    //设置servlet的初始化参数
    WebInitParam[] initParams() default {};

    boolean asyncSupported() default false;

    String smallIcon() default "";

    String largeIcon() default "";

    String description() default "";

    String displayName() default "";
}
```
### ServletConfig对象
* 是Servlet的配置参数，在Servlet规范中,允许每一个Servlet都提供一些相关初始化的方式,每个Servlet都有一个所对应ServletConfig
* 生命周期就是随Servlet而存在
### **ServletContext**
#### 概念
- ServletContext对象,是应用上下文对象
- 每一个web应用有且只有一个ServletContext对象
- 他可以实现让应用中所有的Servlet间的数据共享
- **结论**: 所有的浏览器和Servlet共享同一个资源,也就是共享同一个application对象
#### 生命周期
- 随着服务器的存在而存在，也是单例模式
- 只要应用一直提供服务方法(service)，那么就会一直存在
- 服务器正常关闭该对象就会消失
### 域对象
* 概念：是的是对象有作用于，也就是对象有使用范围
* 作用：用于存取数据，域对象可以实现数据的共享，不同的作用域对象，共享数据的范围不一样
* 在Servlet规范中，一共有4个域对象，ServletContext域对象是作用域最大的，它可以实现这个web应用间的数据共享功能
#### 常用方法
- setAttribute(String name,Object o):向域中存数据
- getAttribute(String name):取出域对象中的数据
- removeAttribute(String name):删除域对象中的数据

## HTTP协议
### 概念
* 全称：超文本传输协议
* 超文本：包含超链接、视频等元素的文件
* 传输协议：
  * 传输：在服务器和客户端(浏览器)之间传输
  * 协议：行为规范，也就是约束，必须要遵守的规范
### 特点
- **默认端口号：80**
- Http协议是基于TCP/IP协议进行实现的
- 基于请求和响应模型：一次请求对应一次响应
- 无状态协议：每次请求之间是互相独立的，不能进行交互数据
### 组成
- 请求：request
  - 请求行 - 请求方式 + URI + 协议版本
    - 请求方式：get / post
    - URI ： 虚拟目录 + servlet资源路径
    - http协议版本
  - 请求头 - 客户端/浏览器的相关信息
    - 都是以map的形式进行存储(key-value)
    - 包含的都是请求客户端的信息
    - 重要的请求头信息：Cookie 和 User-Agent
  - 请求体 - 提交的请求参数
    - 只有post提交的方式，才会将请求的参数放到请求体
    - get方式是不会走请求体的
- 响应：response
#### 请求 - Request
- 发送请求的方式：刷新页面、超链接、按钮...
- 请求参数：在表单中提交参数  
  > 默认方式是Get提交
- 请求方式有：GET 和 POST 两种
  > GET : 直接拼接在连接后，也就是URL后
  > POST : 存在请求体中，也就是只有表单属性method=’post‘时才是post的方式提交
- 获取请求信息数据
```text
+ 获取请求行数据
    - 获取请求方式:        String getMethod()
    - 获取虚拟目录:        String getContextPath()
    - 获取Servlet路径:    String getServletPath()
    - 获取get方式请求参数:  String getQueryString()
    - 获取请求URI:        String getRequestURI()
                         StringBuffer getRequestURL()
    - 获取协议及版本:      String getProtocol()
    - 获取客户机的IP地址:  String getRemoteAddress()
+ 获取请求头数据
    - 通过请求头的名称获取请求头的值  
                        String getHeader(String name)
+ 获取请求体数据的步骤:
    - 获取流对象
        * BufferedReader getReader()
                        :获取字符输入流，只能操作字符数据
        * ServletInputStream getInputStream()
                        :获取字节输入流,可以操作所有类型数据
    - 再从流对象中拿数据
```
- 获取请求参数的通用方式
```text
- 特点:不论get还是post请求方式都可以使用下列方法来获取请求参数
- 根据参数名称获取参数值:   String getParameter(String name)
- 根据参数名称获取参数值的数组:    String[] getParameterValues(String name)
- 获取所有请求的参数名称:   Enumeration<String>  getParameterNames()
- 获取所有参数的map集合:   Map<String,String[]> getParameterMap()
```
- 解决乱码问题
```text
+ get请求默认字符集: 和tomcat一致
  	- 解决乱码方式一:
		new String(username.getBytes("ISO8859-1"), "UTF-8");
+ 解决乱码方式二:
  	- 编辑tomcat中的编码集: conf/server.xml
	  	<Connector URIEncoding="UTF-8"/>
+ post请求默认字符集: ISO8859-1
	- 解决乱码:setCharacterEncoding("UTF-8")
```
#### 响应 - Response
- 接收请求参数，进行相关处理和操作
- 返回响应

### HTTP 与 HTTPS
![](otherPic/HTTP与HTTPS区别.png)
#### 三次握手
![](otherPic/三次握手原理图.png)
- 简单理解就是：确认连接的方式
> - A：喂，听得到吗？
> - B：听得到，你听得到吗？
> - A：听得到，可以开聊了
#### 四次挥手 - 连接终止协议
![](otherPic/四次挥手解析.png)
- 简单理解：
> - A：我没什么要说的了，我这边结束发送了
> - B：好的，了解
> - B：行了，我这边也没什么问题了，结束吧
> - A：收到，结束
### HTTPS工作原理
![](otherPic/HTTPS工作原理图解.png)  
![](otherPic/HTTPS工作原理步骤解析.png)

## 业务开发过程
- 业务开发流程
  数据库 -> 创建表 -> 创建表对应的实体类 -> 前端页面   
  -> 导入相关jar包 -> 真正的开发流程 ->   
  接收请求 -> 处理数据 -> 操作数据库
## URL 与 URI
### URI
- URI:给客户端是使用,或者给服务器端使用
> 给客户端使用: 必须有虚拟目录
> 给服务器使用: 转发,不需要虚拟目录
### URL
- form 中 action = "url"
- a 中 href = "url "
- img 中 src = " url"
- link 中 href = "url "
- script 中 src = " url"
- 重定向中 = "url"  
————————————————————————
- 资源在同一个服务器中,书写的时候可以将URL简写成URI使用  
  例如(javaWeb是虚拟目录):/javaWeb/ResponseDemo01  
- 不在同一个服务器中,只能填完整的URL路径  
   例如: `http://www.baidu.com`
## 会话技术
* 从开启访问某个网站，到关闭网页/浏览器的过程，称为一次会话
* 会话技术就是指在会话中，帮助服务器记录用户状态和数据的技术
* 在不同请求间实现数据的共享
#### http协议
基于请求与响应模式的无状态协议(如302等)
- 协议对于所有的事务处理都没有记忆能力，如果不经过设置，  
  服务器不自动维护用户保存信息的功能，所以无法保存用户状态
### 分类
* Cookie : 客户端的会话技术
* Session : 服务器端的会话技术
#### Cookie
- **特点**：把共享的数据保存到客户端(浏览器)，每次请求时，会把会话信息带到服务器中，实现多次请求间的数据共享  
- **仅支持String数据格式且仅支持英文**
- 设置cookie
- 获取cookie  
> - 使用request对象获取客户端携带的所有的Cookie
> - 遍历数组，获取每一个Cookie对象
> - 使用Cookie对象方法获取数据
#### Session
**Session技术是基于Cookie实现的**
- **特点**：他本质上还是才有Cookie技术进行存储，只不过保存到客户端是使用一个特殊的标记形式  
    并且把要保存的数据保存到服务器的内存对象中
> 直接把数据存储在客户端可能会被窃取或删除，存在很大的安全问题
- 简单理解就是存的地方换了，Cookie是存在本地，Session是存在服务器中
- 概念：浏览器访问Web服务的资源时，服务器会给每个用户浏览器创建一个Session对象，每个浏览器都独占一个Session对象
- **生命周期**：
> 创建：客户端第一次调用getSession()
> 销毁：  
>   - 服务器正常关闭或应用被卸载
>   - session过期：默认情况下无操作则30分组后自动销毁，或者手动session对象.invalidate()销毁
- Session的**钝化** 与 **活化**
> - 钝化：在服务器正常关闭后Tomcat会自动将session数据写入到硬盘文件中(序列化)
> - 活化：当再次启动服务器后，从文件加载数据到session中(反序列化)
> - 注意：序列化的实现需要实现可序列化接口Serializable
#### cookie 与 session 的区别
- 相同点
> 都是完成一次会话内多次请求间的数据共享
- 不同点
> - 数据存储位置：
>   - cookie把数据存在客户端
>   - session把数据存在服务器
> - 安全性
>   - cookie不够安全
>   - session相对安全
> - 数据大小
>   - cookie有限制，大约3~4kb
>   - session无大小限制
> - 存储时间
>   - cookie可以通过setMaxAge(单位：分钟)设置时间
>   - session默认30分钟
> - 服务器性能
>   - cookie不占用服务器内存
>   - session占用服务器内存
> - 应用场景
>   - cookie购物车功能、一段时间免登录
>   - session登录功能中的用户名展示、验证码

## JSP - Java Server Page
* JSP 原理：本质就是一个Servlet
### 用法
- <% Java代码 %> ：内容对应的是service()中的代码
- <%! Java代码 %> ：类中的成员，包括成员变量和成员方法
- <%= Java代码 %> ：输出代码
- <%@ 指令名称 属性名=属性值 %> ：引入
### 九大内置对象
* 在JSP页面中不需要获取和创建，可以直接使用的对象
* 九大对象：
> request：获取用户请求信息  
> response：获取客户端的响应信息  
> out：输出内容到界面上  
#### out
- 有默认缓冲区(8kb、buffer="8kb")，可以设置为0，会直接写入到response缓冲区
- 有缓冲区意外着会比response晚
### 四大域对象
- pageContext - 当前JSP页面 ~ pageContext
> 只有在JSP页面中生效
- request - 一次请求 ~ HttpServletRequest
> - 创建：发送请求
> - 销毁：GC，请求接收后并不会马上销毁，只是没有引用而已
- session - 一次会话 ~ HttpSession
> - 创建：第一次调用getSession()
> - 销毁：非正常关闭服务器、到期超时(30分钟)、手动销毁
- application - 整个web应用 ~ ServletContext
> - 创建：服务器启动就创建
> - 销毁：服务器正常关闭

## MVC 与 JavaEE(JavaWeb)三层架构
### MVC
- M - Model - 模型，业务模型
- V - View - 视图，展示的页面
- C - Controller - 控制器，处理请求，调用模型和视图
> 控制器(Controller)用来接收浏览器发送过来的请求,  
> 控制器会调用模型(JavaBean)来获取数据(比如从数据库中查询数据)  
> 控制器获取到数据后再交给视图(JSP,HTML)来进行数据的展示
### JavaEE 三层架构
- 表现层 - web - 前端的获取和展示，请求的来源
- 业务逻辑层 - service - 表现与数据访问的中间连接
- 数据访问层 - dao - 对接数据库
> 一般资源所存放的包名为：  
> - 实体类：domain、entry(entries)、pojo、bean
> - 服务层：service
> - 数据层：dao
> - 表现层：web(存放各种Servlet)
> - 工具包：utils
> - 测试包：test

## 过滤器 - Filter----------------------------------------------------------
**概念**：Filter表示过滤器，是javaWeb三大组件(Servlet、Filter、Listener)之一  
过滤器可以把对资源的请求拦截下来，实现一些特殊功能，如未登录不能直接访问内部资源
- 作用：一般用于完成一些通用型操作，如登录验证、统一编码集、敏感字符的过滤
- 执行流程：
> **放行前的代码 -> **
- **执行顺序默认是按照类的命名的自然排序**

## Ajax
- Ajax - ASynchronous JavaScript and XML
- Ajax 不是一门技术，而是多种技术的综合，其中包括了JS、XML、JSON等等
- **作用**：Ajax是通过服务器与浏览器进行少量数据交换，就可以使用异步进行数据更新
> 也就是在不重新加载整个页面的情况下，对网页内容进行局部刷新
### 同步 与 异步
- **同步**：服务器在处理请求的过程中，客户端**不能**进行其他操作
- **异步**：服务器在处理请求的过程中，客户端**可以**进行其他操作
### 原生JS实现Ajax
- 核心对象：XMLHttpRequest
  - 打开连接：open()，三个参数如下:
  > - method : 请求的类型，get和post  
  > - uri : 请求的路径  
  > - async : 异步或同步，**异步为true，同步为false**

## JSON - 数据格式
- **概念**：一种轻量级的、基于文本的、开放的 **数据交互格式**
- 存储的形式是**键值对**
> JSON是一种纯字符串的数据，本身不提供任何的方法
### 语法格式
- 格式定义：var 变量名 = '{"key1": "value1", "key2": "value2"}'
- 键：需要写在双引号中
- 值：
> - 简单类型
> > - 数字(整型和浮点型)
> > - 字符串(只能用双引号，不能用单引号)
> > - 逻辑值(true / false)
> > - null (null)
> - 复杂类型
> > - 数组：写在 [ ] 中
> > - 对象：写在 { } 中
### JS 与 JSON
- JS 中可以使用单引号和双引号
- JSON 中**不能**使用单引号，不识别，只能使用双引号

## Redis
- **概念**：是用C语言开发的一个开源的、高性能的、以键值对的形式存储的数据库
- 特点：
> - 基于内存存储，读写性能高
> - 支持持久化，可以进行数据恢复
> - 适用于存储常用数据，减少资源的消耗
- 使用场景：
> - 为热点数据提升查询效率(新闻、咨询、热点商品)
> - 即时信息查询(直播人数统计、排行榜)
> - 时效性信息控制(验证码)
### Redis 数据类型
- 字符串（string）：普通字符串，常用类型
> - set key value ：设置指定的key和value(用于添加)
> - get key ：获取指定key的值(单个获取)
> - setex key seconds value ：设置指定key和value并给上生命时长，单位是秒，到点就过期，如验证码过期
> - 通用命令：
>   - keys * ： 查询所有的key
>   - type key ： 查询指定key的value的数据类型
>   - del key ： 删除指定的key value
- 哈希（hash）：map格式，主要用于存储对象
- 列表（list）：按照插入顺序，可以有重复值
- 集合（set）：无序集合，唯一
- 有序集合（sorted set / zset）：集合中每个元素都有个关联分数（score），跟主键一样，可以用于取数据，根据score升序排序，唯一

## Maven
### 坐标
- maven中的坐标是获取资源(jar包)的唯一标识，用于表述仓库中的资源位置
- -------------------------

## 框架
**框架**：就是一个经过验证并且具有一定通用性的半成品软件
### 框架的作用
- 提高开发效率(基于半成品软件进行二次开发)
- 提供编写规范
- 节约维护成本
- 解耦底层实现(核心)
### 常见框架
* 持久层框架(dao层)： Mybatis、Spring JDBC
* 表现层框架(web)： SpringMVC
* 全栈框架(三层都有)： Spring
#### Mybatis框架
**概念**：是一个基于ORM思想实现的 一个**半自动轻量化持久层框架**，用来操作数据库的
> MyBatis就是JDBC原石操作过程的封装，让开发者只需要关注SQL本身，而不需要进行其他操作，如注册驱动等
- 原始JDBC连接存在的问题
> - 频繁的创建和销毁数据库连接，造成系统资源的浪费，影响程序性能
> - sql语句在代码中是以硬编码的形式存在，若要修改sql语句，还得修改dao的类，不易维护
> - 查询数据还得手动封装结果，非常便捷
> - 增删改时还得手动传参，如需要传参到占位符？
- 解决方式
> - 使用数据库连接池优化连接
> - 将sql语句抽取到xml配置文件中
> - 映射关系优化，用反射机制完成，将实体类和表之间，属性与字段自动映射(同名会自动识别)
> > 涉及ORM思想，通过ORM完成映射
- ORM 思想 - Object Relational Mapping 对象关系映射
> Object 对象模型：实体对象，程序中的JavaBean实体类  
> Relational 关系型数据库的数据结构：数据库表  
> Mapping 映射：从数据表(Relation)到对象(Object)的映射，可以通过xml的方式映射  
> 实现：  
> - 先让实体类和数据库表一一对应
> - 再让实体类**属性**和数据库**字段**一一对应
- **核心对象**
> - 核心接口和类(常用API)
> - Mybatis核心配置文件(mybatis-config.xml)
> - SQL映射文件(mapper.xml)
- 开发步骤
> - 创建数据库与表
> - 创建项目，导入相关jar包
> - 创建实体类
> - 编写映射文件
> - 编写核心配置文件
> - 编写测试类进行测试
- Mybatis映射配置文件
> - 映射器是Mybatis中最重要的文件，文件中包含了一组SQL语句(增删改查)，这些语句被称为**映射语句**、**映射SQL语句**  
> - 映射器由Java **接口** 和 **XML** 文件(或 **注解**)共同组成
> - **作用**：
>   > - 定义参数类型
>   > - 提供**SQL语句** 和 **动态SQL**语句
>   > - 定义**查询结果** 和 **POJO**(实体对象) 的映射关系
> - 映射器的实现方式：
> > - 通过XML方式实现
> > > dao层接口还是要写，但是对应的实现类Impl不写，转写对应的Mapper.xml  
> > > - mybatis中的占位符是‘#{对应字段名}’
> > > ```xml
> > > <mapper>
> > >     <insert id="insert" parameter="">
> > > -----------------------------------------------------------
> > >     </insert>
> > > </mapper>
> > > ```
> > - 通过注释方式实现
> - **配置文件的标签顺序**(固定,顺序错了会报错)
> > - configuration（配置） 
> >   - properties（属性） 
> >   - settings（设置） 
> >   - typeAliases（类型别名） 
> >   - typeHandlers（类型处理器） 
> >   - objectFactory（对象工厂） 
> >   - plugins（插件） 
> >   - environments（环境配置） 
> >     - environment（环境变量） 
> >       - transactionManager（事务管理器） 
> >         - dataSource（数据源） 
> >   - databaseIdProvider（数据库厂商标识） 
> >   - mappers（映射器）

    <configuration>:核心根标签
    <properties>:引入数据连接信息等配置文件[加载外部的properties文件]
    <typeAliases>:设置类型别名[给自定义类起别名]
    <environments>: 配置数据库环境[配置数据源]
    <mappers>:加载映射配置文件

- Mybatis高级查询
> - resultType属性:如果实体的属性名和表中字段名一致,将查询结果自动封装到实体类中. 
> - resultMap属性:如果实体的属性名和表中字段名不一致,需要使用resultMap进行手动封装.
> - 多条件查询
> > - 方式一：  
      使用#{arg0} - #{argn}  
      使用#{param1} - #{paramn}  
> > - 方式二:  
      使用注解,引入 @param()注解获取参数
> > - 方式三:  
      使用pojo(实体类对象)传递参数
> - 模糊查询
> > - 方式一: 使用#{}
> > - 方式二: 使用${}

#{} 和 ${}的区别:
- #{}:表示一个占位符  
  可以通过#{},实现PreparedStatement向占位符中设置值,自动进行从java类型到jdbc类型的转换,#{}可以**有效防止**sql注入 
> - #{} 可以接收简单类型值或者pojo类型的属性值  
> - 如果parameterType传入的是单个简单类型的值, #{}中的名称可以随意填写,例如:#{id}  
> - #{} 使用时**会**自动拼接上''(单引号)
- ${}:表示原始拼接sql字符串  
  通过${}可以将parameterType传入到内容拼接的sql中不会进行从java类型到JDBC类型的转换.并且会**出现sql注入问题**
> - ${} 可以接收简单类型值或者pojo类型的属性值 
> - 如果parameterType传入的单个简单类型的值,${}中的值只能是value,例如:'${value}'
> - ${}使用时**不会**自动拼接''(单引号)

- Mybatis加载策略
> - 延时加载(懒加载)：加载当前对象时并没有立即把关联对象查出，而是有需求时才加载，如User对象中的属性Order是另一张表，不会立即查出
> > **延时加载是基于嵌套查询实现的**
> > - 优点：先从单表查询，有需求时再进行多表关联查询，提高了查询效率，性能较高
> > - 缺点：由于需要时再查，当数据量大时，用户需要消耗**时间成本**去等待数据查询出结果
> > - 使用场景(多表关系中)：**一对多**、多对多(封装时其实多对多会转化为一对多)
> - 立即加载：加载当前对象时就立即把关联对象查出，与延时加载对应
> > 使用场景：常用于一对一关系中，因为数据量小，直接就加载了
- Mybatis缓存
> - 概念：存储一些经常被查询而且又不经常发生变化的数据，用来提高效率
> - 分类：一级缓存和二级缓存
>   - 一级缓存 - 是SqlSession级别的缓存，是Mybatis的默认缓存级别
>   > 一级缓存清空：因为一级缓存是SqlSession范围的缓存，所以执行增删改或调用clearCache()、commit()、close()都会清空
>   - 二级缓存 - 全局缓存，也被称为namespace级别缓存(跨SqlSession的缓存)，可以被所有的SqlSession所共享
>   > - 一级缓存存的是SQL语句，而二级缓存存的是结果对象
>   > - 二级缓存默认是不开启的，若要开启则需要进行配置
>   > - 若要实现二级缓存，要求返回的POJO对象(实体对象)必须是可序列化的

##### Mybatis 中XML标签

    <mapper>:核心根标签
        namespace属性: 命名空间,唯一标识

    <select>:查询功能
        属性:
            id: 唯一标识,用于配合命名空间使用
            parameterType: 指定参数映射的对象类型
            resultType: 指定结果映射的对象类型

    <insert>:新增功能
        属性:
            id: 唯一标识,用于配合命名空间使用
            parameterType: 指定参数映射的对象类型
            resultType: 指定结果映射的对象类型

    <update>:修改功能
    <delete>:删除功能
    <ResultMap>标签:实现手动映射装配
        属性:
            id:唯一表示 
            type:封装后的实体类型

        <id>标签:封装表中主键字段
		    property: 要封装的实体类的属性名
			colum: 表中字段名
		<result>标签:封装表中普通字段
			property: 要封装的实体类的属性名
			colum: 表中字段名

##### 代理模式开发
// 是**解决**Mybatis集成Dao时产生的模版代码重复的问题
- 使用代理模式必须遵循的规范:
>  - 映射配置文件中，**命名空间**必须和Dao层中接口的**全限定类名**相同(namespace = 接口名)
>  - 映射配置文件中，增删改查标签中的**id**必须和Dao层接口中**方法名**相同(id = 方法名)
>  - 映射配置文件中，增删改查标签中的**parameterType属性**必须和Dao层中接口**方法的参数**相同(parameterType = 参数类型)
>  - 映射配置文件中，增删改查标签中的**resultType属性**必须和Dao层中接口方法的**返回值**相同(resultType = 返回值类型)

##### 动态SQL
**Mybatis最强大的特性之一**
- 条件判断标签`<if>`
> - if 标签类似于Java中的 if 语句，是MyBatis中最常用的判断语句 
> - 使用 if 标签可以节省许多拼接SQL的工作，把精力集中在XML的维护上.
- 条件标签`<where>`
> where 标签主要用来简化SQL语句中的条件判断，可以自动处理and/or条件
- 动态更新标签`<set>`
> - 在 Mybatis 中，update 语句可以使用 set 标签动态更新列 
> - set 标签可以为 SQL 语句动态的添加 set关键字，剔除追加到条件末尾多余的逗号
- 循环遍历标签`<foreach>`
> 主要用来做数据的循环遍历
> - open:代表语句要开始部分, 既然是 in 条件语句，所以必然以 ( 开始 
> - close:代表语句要结束部分, 既然是 in 条件语句，所以必然以 ) 开始 
> - separator:代表在每次进行迭代之间以什么符号作为分隔符,既然是 in 条件语句，所以必然以【,】 作为分隔符. 
> - item:代表要遍历结合中的元素每个元素,要生成的变量名 
> - index：指定一个名字，表示在迭代过程中每次迭代到的位置 
> - collection:代表要遍历的集合元素,通常写 collection 或者 list 或者 map key

注意:用 foreach 标签时，最关键、最容易出错的是 collection 属性,该属性是必选的，但在不同情况下该属性的值是不一样的主要有3 种情况：
> - 如果传入的是单参数且参数类型是一个 List，collection 属性值为 list。 
> - 如果传入的是单参数且参数类型是一个 array 数组，collection 的属性值为 array。 
> - 如果传入的参数是多个，需要把它们封装成一个 Map，单参数也可以封装成Map
>   > Map 的 key 是参数名，collection 属性值是传入的 List 或 array 对象在自己封装的 Map 中的 key。

- SQL提取的定义标签`<sql>`  
`<sql id = "片段唯一标识">抽取的SQL语句</sql>`
- SQL提取的调用标签`<include>`  
`<include refid="片段唯一标识" />`

##### 分页插件
- 高级标签 - `<plugins>`标签
> Mybatis可以使用第三方的插件来完成功能的扩展,使用分页助手PageHelper完成分页中复杂的操作的封装.

- 使用步骤:
> - 在pom.xml中导入PageHelper的坐标 
> - 在mybatis核心配置文件中使用<plugins>标签配置PageHelper插件 
> - 测试分页数据

- 分页助手相关 API
> PageHelper：分页助手功能类。

##### 多表查询
- 一对一
> 通过`<resultMap>`元素的子元素`<association>` 处理一对一级联查询:  
> `<association>` 元素中通常使用以下属性：
> - property ：指定映射到实体类的对象属性
> - column ：指定表中对应的字段(即查询返回的列名)
> - javaType：指定映射到实体对象属性的类型
- 一对多
> 通过`<resultMap>`元素的子元素`<collection>`处理一对多级联关系collection 可以将关联查询的多条记录映射到一个 list 集合属性中  
> `<collection>` 元素中通常使用以下属性:
> - property ：指定映射到实体类的对象属性
> - column ：指定表中对应的字段(即查询返回的列名)
> - javaType ：指定映射到实体对象属性的类型
- 多对多
> 实际上多对多的关系需要转化为一对多来进行数据封装，所以参照一对多即可

##### 注解开发
- 注解分类：
> - SQL语句映射
> - 结果集映射
> - 关系映射
- SQL语句映射
> - @Insert:实现新增功能,代替了`<insert>`标签
> - @Delete:实现了删除功能,代替了`<delete>`标签
> - @Update:实现了更新功能,代替了`<update>`标签
> - @Select:实现了查询功能,代替了`<select>`标签
> - @Param:映射多个参数,用在Mapper接口中映射多个参数
- 结果集映射
> - @Result:实现结果集的封装,代替了`<result>`标签
> - @Results:可以和@Result结合使用,封装多个结果集,代替了`<resultMap>`标签
>   - 封装多个结果集:@Results({@Result(),@Result()})
>   - 封装单个结果集:@Results(@Result())
- 关系映射
> - @One: 用于一对一关系映射,代替了`<association>`标签 
> - @Many: 用于一对多关系映射,代替了`<collection>`标签

### Spring
> Spring 是分层的Java SE/EE应用full-stack轻量级开源框架  
以 **IoC(Inverse Of Control：反转控制)**和**AOP(Aspect Oriented Programming：面向切面编程)** 为内核，
提供了展现层 Spring MVC 和持久层 Spring JDBC 以及业务层事务管理等众多的企业级应用技术，
还能整合开源世界众多著名的第三方框架和类库，逐渐成为被使用最多的 Java EE 企业应用开源框架。  
	Spring框架是JavaEE开发必学的技术,因为企业开发技术选型中占比达到95%

AOP在Spring中做得是**事务管理**,如业务增强
- 优点：
> - 简化开发
> - 框架整合

- IOC 思想：使用对象的时候,在程序中不主动使用new来产生对象,交给外部(IOC容器)来进行提供对象,这种称之为IOC思想

- Spring目的：充分解耦合
> - 使用IOC容器管理Bean对象(IOC 思想)
> - 在IOC中将所有存在依赖关系的Bean进行绑定(DI 思想)

#### IOC - Inverse Of Controller - 反转控制
// 把创建对象的过程交给Spring进行管理，我们只需要通过Spring获取到对象  
Spring中对于IOC**思想**的实现：
- 在Spring中创建了一个IOC容器，当做IOC思想的外部
- IOC容器负责所有Java对象的**创建、初始化**等一系列工作，被创建或被管理的对象在IOC容器中被称为Bean
- Bean对象是IOC中管理的对象，也叫Spring Bean
> 这个Bean对象跟平时new的对象没什么区别，就只是交给Spring管理、我们不需要自己创建而已

- IOC 使用步骤：
> - 在pom.xml文件中导入spring相关依赖(jar包)
> - 定义Spring管理类(接口)
> - 创建Spring相关配置文件,配置对应类作为Spring管理的bean对象
> - 初始化IOC容器

##### IOC的工作原理
IOC底层下通过**工厂模式**、**Java的反射机制**、**XML解析**等相关技术,将代码的**耦合度降到最低**,比如说:
- 在配置文件(applicationContext.xml)中对**对象**以及他们之间的**依赖关系**进行配置
- 可以把IOC容器当成一个**工厂**,这个工厂生产的产品就是Sprig Bean
- 当容器启动时就会加载并解析这些配置文件,得到对象的基本信息以及他们之间的依赖关系
- IOC就是使用了Java的**反射技术**,根据类名生成相应的对象(也就是Bean),并根据依赖关系将这个对象注入到依赖它的对象中
- 由于对象的基本信息,对象之间的**依赖关系都是在配置文件中定义的**,所以代码的耦合度低,所以当对象发生改变,我们只需要在配置文件中进行修改即可.这也是Spring IOC实现解耦的原理  

总结:Spring框架中用工厂模式实现了IOC容器

#### 依赖注入 - DI
// 在IOC容器中，建立Bean与Bean之间的依赖关系这个**过程**叫做依赖注入(DI)
> 也就是引用类型的成员变量的赋值

- DI 使用步骤：
> - 删除之前使用new的方式创建对象的相关代码(符合IOC)
> - 提供依赖对象所需要的set方法,实现依赖注入(给Spring提供注入的路)
> - 配置service与dao之间的关系(告诉Spring注入的方向)
> - 编写测试类进行测试

- 注入方式：
> - 普通方法注入，setter注入，也叫设值注入
> - 构造方法注入
- 注入的数据类型
> - **引用数据类型**
> - 简单类型(**基本数据类型**和**String**)
> - 集合类型(**集合**和**数组**)


##### Setter注入
可以通过Bean的setter()将属性值注入到Bean的属性中
- 注入过程：
> - 在Spring实例化过程中，IOC容器首先会调用默认的无参构造方法实例化对象
> - 然后通过Java的反射机制调用这个Bean的setXxx()将属性注入其中
##### 构造器注入
通过Bean的带参构造方法,实现Bean的属性注入
> 注意:在Bean中添加一个有参构造方法,构造方法中每一个参数都代表一个需要注入的值
##### 自动注入
- IOC容器根据bean所依赖的资源内容在容器中自动查找并注入到bean对象中的过程称为自动装配
- 自动装配的取值：
> - byType：按照类型装配
> - byName：按照名称装配
> - constructor：按照构造方法中的参数类型装配
- 注意事项：
> - 自动装配用于引用数据类型的依赖注入，不能对简单类型进行操作
> - 按照类型装配(byType)时，必须保证整个IOC容器中相同类型的bean有且只有一个
> - 优先级：setter注入，构造方法注入 > 自动装配
#### 配置文件模块化
`<import resource="副模块.xml" />`
- 同一个xml文件中bean的id不能有同名
- 多个xml文件中bean的id可以同名，后加载的会覆盖先加载的同id的bean

#### Spring Bean
- Bean 命名
> name属性: 该属性表示 Bean 的名称，我们可以通过 name 属性为同一个 Bean 同时指定多个名称  
> 每个名称之间用逗号`,`或分号`;`或空格 分隔  
> Spring 容器可以通过 name 属性配置和管理容器中的 Bean
- Bean 作用范围配置
> scope属性: 表示 Bean 的作用域（作用范围）
> > 属性值:
> > - singleton:默认值,**单例模式**
> > - prototype:原型模式,**非**单例，要一个创一个
> > 适合交给IOC容器进行管理的bean (**适合**单例模式)
> > - 表现层对象(servlet)
> > - 业务层对象(service)
> > - 数据层对象(dao)
> > 不适合交给IOC管理的Bean (**不**适合单例)
> > - 实体类的对象(bean/pojo/domain/entry)

- Bean 实例化：虽然是交给外部，但本质上还是创建对象，所以使用**构造方法**完成
> - 构造方法创建
> - 静态工厂创建
> - 实例工厂创建

- Bean 生命周期
> 实例化 ——> 属性赋值 ——> 初始化 ——> 相关调用 ——> 销毁

> 被IOC管理的Bean对象作用域是整个应用，而非单例的对象，IOC只管创建，不管后序的生命周期

- 自定义Bean生命周期 - 回调方法
> 实现回调方法的方式：
> - 通过接口方式实现 
> - 通过xml配置实现 
> - 通过注解实现

## 注解开发
目的：为了**简化开发**,使用类+注解的形式优化掉xml配置bean对象定义的过程

### Bean 对象
- @Component注解: 定义bean对象 - 标记在类上
> 衍生三个注解:
> - @Controller: 用于表现层(web)定义bean
> - @Service: 用于业务层(service)定义bean
> - @Repository: 用于数据层(dao)定义bean

- @Scope注解: 定义bean的作用范围
- 定义Bean的生命周期 - 定义在方法上
> - @PostConstruct: bean的初始化
> - @PreDestroy: bean的销毁

### DI 注入
- @Autowired:配置**引用类型**成员变量,自动装配,**根据类型**配置
> - @Qualifier:必须依赖于@Autowired使用,**根据名称**装配
- @Value:配置**简单类型**
- properties配置文件加载属性
> - 核心配置文件中添加@PropertySource 
> - 使用${key},获取配置文件中对应的value

### 第三方 Bean
- 第三方bean管理
> @Bean:声明并使用第三方的bean
- 第三方的依赖注入
> - 引用类型:方法形参
> - 简单类型:成员变量

### 注解分类
- SQL语句映射
- 结果集映射
- 关系映射
### SQL语句映射
- @Insert：实现新增功能，代替了`<insert>`标签
- @Delete：实现了删除功能，代替了`<delete>`标签
- @Update：实现了更新功能，代替了`<update>`标签
- @Select：实现了查询功能，代替了`<select>`标签
- @Param：映射多个参数，用在Mapper接口中映射多个参数
### 结果集映射
- @Result：实现结果集封装，代替了`<result>`标签
- @Results：可以和@Result结合使用，封装多个结果集，代替了`<resultMap>`标签
### 关系映射
- @One：用于一对一关系映射，代替了`<association>`标签
- @Many：用于一对多关系映射，代替了`<cellection>`标签

### Spring 集成 Mybatis
- 分析
> - 使用SqlSessionFactory封装SqlSessionFactory所需要的环境信息
> - 封装Mapper对象

- 实现步骤
> 1. 创建数据库与数据表 
> 2. 编写实体类,封装数据 
> 3. 编写业务层接口与实现类 
> 4. 编写数据层接口(注解实现增删改查)
> 5. pom.xml中导入Spring整合mybatis相关依赖 
> 6. 创建配置类JdbcConfig 配置dataSource数据源，创建jdbc.properties配置文件 
> 7. 创建MybatisConfig配置文件整合Mybatis 
> 8. 创建SpringConfig主配置类进行包扫描和加载其他配置类 
> 9. 定义测试类进行测试

### 面向切面编程 - AOP - Aspect Oriented Programming
// 面向切面编程，通过预编译方式和运行期间动态代理实现程序功能的统一维护的一种技术。  
AOP是OOP的延续，是软件开发中的一个热点，也是Spring框架中的一个重要内容，是函数式编程的一种衍生范型。利用AOP可以对业务逻辑的各个部分进行隔离，从而使得业务逻辑各部分之间的耦合度降低，提高程序的可重用性，同时提高了开发的效率

- 作用：在不改变方法源代码的基础上对方法进行**增强**
> Spring倡导的理念: 无侵入式开发

#### 核心概念
- 连接点(JoinPoint)：AOP的核心概念,指的是程序执行期间明确定义的一个点.
> 例如：方法的调用、类初始化、对象实例化、设置变量、抛出异常等。  
> 注意:在 Spring中,连接点则指可以被动态代理拦截目标类的方法,理解为方法的执行
- 切入点(Pointcut)：又称切点,指要对哪些 连接点 进行拦截，增强。
> 注意：在SpringAOP中,一个切入点可以只描述一个具体方法,也可以匹配多个方法.
- 通知(Advice)：指拦截到 连接点 之后要执行的代码，即对切入点增强的内容。
> 注意:在Spring AOP中,功能最终以方法的形式呈现.
- 通知类：通知方法所在的类叫做通知类
- 切面(Aspect)：描述通知与切入点的对应关系,也就是哪些通知方法对应哪些切入点方法。
- 目标对象(Target Bean):被代理的对象,也称之为原始对象,该对象中的方法本身是不具备有任何增强的功能. 
- 代理(Proxy):代理后生成的对象,是由Spring帮我们创建代理对象.
> 代理方式两种:
> - JDK 动态代理 
> - CGLIB 动态代理

#### 使用步骤
- 在pom.xml中导入坐标(jar包) 
- 制作连接点方法(原始方法,dao接口与实现类)
- 制作共性功能(编写通知类与通知)
- 定义切入点(指定功能追加给谁)
- 绑定切入点与通知的关系(切面)

#### AOP工作流程
- Spring容器启动
- 读取所有切面配置的切入点
- 初始化bean,判断当前bean对应的类中的方法是否能匹配到切入点
> - 匹配失败,创建原始对象(目标对象),也就是初始化bean 
> - 匹配成功,创建原始对象(目标对象)的代理对象  
> 注意:走的是AOP代理模式

- 获取bean执行方法
> - 获取bean中原始对象时,调用方法并执行,没有完成功能增强.
> - 获取bean的是代理对象时,根据代理对象的运行模式进行原始方法的内容增强,完成操作.

#### AOP切入点表达式
- 语法格式
> - 切入点: 要进行增强的方法
> - 切入点表达式: 要进行增强的方法的描述方式
> > - 描述方式一: 按照接口进行描述   
> > ps: void com.sushi.dao.BookDao.save()
> > - 描述方式二: 按照实现类进行描述   
> > ps: void com.sushi.dao.impl.BookDaoImpl.save()
> - 标准切入点表达式语法:
> > 访问修饰符 返回值 包名.类名/接口名.方法名(参数) 异常名 
> > - 访问修饰符:public，private等可以省略不写 
> > - 返回值: 写返回值类型 
> > - 包名:多级包使用【.】进行连接 
> > - 类名/接口名 
> > - 方法名: 要增强的方法名 
> > - 参数: 直接写参数的类型,多个参数使用逗号隔开 
> > - 异常名:方法定义中抛出指定异常,可以省略

- 通配符: `*`
> - `*`：单个独立的任意字符，可以独立出现，也可以作为前缀或者后缀匹配符出现
> - `..`：多个连续的任意符号,可以独立出现,一般用于简化包名和参数名
> - `+`：专门用于匹配子类类型

#### AOP通知类型
通知类型5种类型(对应5种位置):
- 前置通知:
- 后置通知
- **环绕通知**(重点)
> 前置和后置组合就是简单的环绕了
- 返回后通知
- 抛出异常后通知

#### AOP切入点的数据获取
- 获取参数
> - JoinPoint: 适用于前置,后置,返回后,抛出异常通知 
> - ProceedingJoinPoint :专门适用于环绕通知
- 获取返回值 
> - 环绕通知(重点) 
> - 返回后通知
- 获取异常 
> - 环绕通知(重点) 
> - 抛出异常后通知

### Spring 事务管理
#### 概念
- 事务(Transaction)是基于关系型数据库实现的重要组成部分.
- 作用:用来确保应用程序中数据的完整性和一致性.也就是保证在数据层或者业务层的数据操作时,能同时成功或同时失败.

#### Spring事务管理方式
- 编程式事务管理 
- 声明式事务管理
#### 事务传播行为
概念:事务传播行为指的是当一个事务方法被另一个事务方法调用时,这个事务方法应该如何运行的过程称之为事务传播行为.


## SpringMVC
概念：SpringMVC是Spring提供的一个基于MVC设计模式的轻量级web开发框架，本质上就是Servlet
- 优点：
  > 基于MVC设计模式，有明确的分工，做到了最大程度的解偶合  
  > 因为是基于Spring的延伸，所以IOC和AOP的特性在SpringMVC也适用
### 三大组件
**处理映射器、处理适配器、视图解析器**  
常用组件如下：
- DispatcherServlet：前端控制器
- HandlerMapping：处理器映射器
- Handler：处理器 (程序员编写方法)
- HandlerAdapter：处理器适配器
- View Resolver：视图解析器
- View：视图

### XML 配置SpringMVC
- 创建web项目,导入SpringMVC相关坐标(jar包)
- 在web.xml中配置SpringMVC的前端控制器
- 编写Controller类和视图页面(jsp/html)
- 使用注解配置Controller中的业务方法和映射路径
- 配置SpringMVC核心文件 spring-mvc.xml

### 注解配置SpringMVC
- @Controller注解
> 类型:类注解  
> 位置:SpringMVC的控制器类定义的上方  
> 作用:设置SpringMVC的核心控制器的bean对象

- @RequestMapping注解
> 类型 / 作用:  
> 类注解: 设置映射请求的请求路径的初始信息  
> 方法注解: 设置映射请求的请求路径的具体信息  
> 位置:SpringMVC控制器类上方 / 方法定义的上方
> - value属性 
> > - value属性是通过 请求的请求地址匹配请求映射. 
> > - value属性是一个字符串类型的数组,表示该请求映射能够匹配多个请求地址所对应的请求 
> > - value属性是必须设置的,否则找不到映射地址. 
> - method属性 
> > - method属性是通过 请求的请求方式匹配请求映射. 
> > - method属性是一个RequestMethod数组类型,表示请求映射能够匹配多种请求方式的请求 
> - params属性 
> > - params属性是通过 请求的请求参数匹配请求映射. 
> > - params属性是一个字符串类型的数组,表示该请求映射能够匹配多个请求参数

- @RequestMapping的派生注解  
> 处理指定的请求方式的控制器方法提供了以下注解:
> - 处理get请求的映射: @GetMapping 
> - 处理post请求的映射: @PostMapping 
> - 处理put请求的映射: @PutMapping 
> - 处理delete请求的映射: @DeleteMapping

### ant 风格路径
// 使用特殊字符对访问路径进行模糊匹配.  
特殊字符:
> - `？`：表示单个字符
> - `*`：表示任意个字符(0 或 n)  
> 注意: 有两个字符不能进行匹配
> - `/` : 表示一层目录
> - `?` : 表示拼接的请求参数
> - `**`：表示任意的一层或者多层  
> 注意: 使用`**`的前后不能有字符,否则`**`会按照单个`*`进行解析

### 路径中的占位符
占位符: `{ }`
> SpringMVC中的占位符常用于restful风格中.当请求路径中将某些数据通过路径的方式传输到服务器中,就可以在相应的 @RequestMapping注解的value属性中通过占位符{xxx}表示传输的数据  
> 再通过@PathVariable注解,将占位符所表示的数据赋值给控制器方法的形参上.

### Controller 控制器加载 - Bean
- SpringMVC控制的bean(表现层的bean)
- Spring控制的bean
> - 业务层的bean(service)
> - 功能相关bean(Datasource)
> - 数据层的bean(dao/mapper)

- 解决方式
> - 使用细粒度加载方式,也就是单个模块包进行加载 
> - 扫描总包,然后将SpringMVC控制的bean排除在外

### 请求
#### 请求参数类型
- 普通类型参数
> 普通参数：url地址传参，地址参数名与形参变量名相同，定义形参即可接收参数  
> 注意: 当请求参数名与形参变量名不同，使用@RequestParam绑定参数关系

- pojo类型参数(实体类)
> pojo参数：请求参数名与形参对象属性名相同，定义pojo类型形参即可接收参数  
> 注意:请求参数key的名称要和pojo中属性的名称一致，否则无法封装。

- 嵌套pojo类型参数(参数的成员变量也是自定义实体类)
> 嵌套POJO参数：请求参数名与形参对象属性名相同，按照对象层次结构关系即可接收嵌套POJO属性参数

- 数组类型参数
> 数组参数：请求参数名与形参对象属性名相同且请求参数为多个，定义数组类型即可接收参数

- 集合类型参数
> 集合保存普通参数：请求参数名与形参集合对象名相同且请求参数为多个，@RequestParam绑定参数关系  
> 注意:url传递参数过程中,只要是名字能对应上的,都可以直接传递,如果名称不一致,则使用@RequestParam对应即可.

#### Json 数据参数传递
- json普通数组:  `["","",","",....]`
- json对象: `{key:value,key:value....}`
> pojo参数：json数据与形参对象属性名相同，定义pojo类型形参即可接收参数
- json对象数组:  
`[{key:value,key:value....}, {key:value,key:value....}, {key:value,key:value....}]`
> pojo集合参数：json数组数据与集合泛型属性名相同，定义List类型形参即可接收参数

#### 日期类型参数
常见格式:
- 2048-08-08
- 2048/08/08(标准格式)
- 08/08/2048
> 注意: 不安标准格式传值,就会出现转换错误,可以使用@DateTimeFormat注解设置日期类型数据格式.

### 常用注解
- @RequestParam注解
> - 类型:形参注解
> - 位置：SpringMVC控制器方法形参定义前面
> - 作用：绑定请求参数与处理器方法形参间的关系

- @EnableWebMvc注解
> - 类型：配置类注解
> - 位置：SpringMVC配置类定义上方
> - 作用：开启SpringMVC多项辅助功能

- @RequestBody注解
> - 类型：形参注解
> - 位置：SpringMVC控制器方法形参定义前面
> - 作用：将请求中请求体所包含的数据传递给请求参数，此注解一个处理器方法只能使用一次

- @DateTimeFormat注解
> - 类型：形参注解
> - 位置：SpringMVC控制器方法形参前面
> - 作用：设定日期时间型数据格式
> - 属性：pattern -> 指定日期时间格式字符串

- @ResponseBody注解
> - 类型：方法注解
> - 位置：SpringMVC控制器方法定义上方
> - 作用：设置当前控制器返回值作为响应体
> > - 返回值是String，内容就是响应体
> > - 返回值是对象,就会转成可识别的数据,再进行响应
> > - 有了这注解才能激活类型的自动转换

#### @RequestParam 和 @RequestBody的区别
- @RequestParam:用于接收url地址参数,或者表单传递的参数
- @RequestBody:专门用于接收json数据

### 响应
响应的方式有：
- 页面跳转(响应页面)
> - 返回字符串逻辑视图(重定向 / 转发)
> - void原始操作ServletAPI
> - ModelAndView

- 返回数据(响应数据)
> 直接返回字符串数据(文本数据,json)

### RestFul 风格 - Representational State Transfer
#### 概述
RESTFul(Representational State Transfer):表现层资源表述状态转移
> rest风格,描述的就是一种访问资源的方式

个人理解其实就是把一小部分的繁杂省掉而已，毕竟是**通过提交方式去寻找对应请求**的，所以有一定的局限性

#### rest的访问方式和以前访问方式的对比
以前访问方式:
> http://localhost/user/getById?id=1    
> http://localhost/user/byId?id=1  
> http://localhost/user/deleteById?id=1  
> http://localhost/user/deleteById?ids=1

rest的访问方式:
> http://localhost/user/1  
> http://localhost/user/1

#### rest风格语法
- URL只能用来标识和定位资源,不可以包含任何与操作相关的动态词
> 例如: 查询全部-> http://localhost/user

- 当请求中需要携带参数时,rest允许我们将参数通过 / 拼接到URL中,将请求参数作为URL的一部分传输到服务器中, 
  而不是像以前一样使用?拼接键值对的方式来携带参数.
> 例如: 根据id查询-> http://localhost/user/1

- HTTP协议中有4个代表操作方式的动态词:
> - GET:用来获取资源(查询)
> - POST:用来新建资源(新增)
> - PUT:用来更新资源(修改)
> - DELETE:用来删除资源(删除)


#### 常见注解
- @PathVariable注解
> - 类型：形参注解
> - 位置：SpringMVC控制器方法形参定义前面
> - 作用：绑定路径参数与处理器方法形参间的关系，要求路径参数名与形参名一一对应

- @GetMapping、@PostMapping、@PutMapping、@DeleteMapping
> - 类型：方法注解
> - 位置：基于SpringMVC的RESTFul开发控制器方法定义上方
> - 作用：设置当前控制器方法请求访问路径与请求动作，每种对应一个请求动作，例如@GetMapping对应GET请求

- @RestController
> - 类型：类注解
> - 位置：基于SpringMVC的RESTFul开发控制器类定义上方
> - 作用：设置当前控制器类为RESTFul风格，等同于@Controller与@ResponseBody两个注解组合功能

#### @RequestBody、@RequestParam、@PathVariable区别和应用
区别:
> - @RequestParam用于接收url地址传参或表单传参
> - @RequestBody用于接收json数据
> - @PathVariable用于接收路径参数，使用{参数名称}描述路径参数
应用:
> - 发送请求参数超过1个时，以json格式为主，@RequestBody应用较广
> - 如果发送非json格式数据，选用@RequestParam接收请求参数
> - 采用RESTFul进行开发，当参数数量较少时，例如:1个，可以采用@PathVariable接收请求路径变量，通常用于传递id值

## SSM 整合 - Spring + SpringMVC + Mybatis
### 配置类整合
> Spring
> - SpringConfig
> MyBatis
> - MyBatisConfig 
> - JdbcConfig
> - jdbc.properties
> SpringMVC
> - SpringMvcConfig
> - ServletConfig

### 功能模块整合
- 表和实体类
- dao层(接口 + 自动代理)
- service层(接口 + 实现类)  
  业务层接口(测试Junit)
- controller层(实现类)  
  表现层实现类(测试PostMan)

### 表现层数据封装
> 目的: 统一响应结果的格式

使用步骤:
- 定义Result类封装响应结果
> 属性分析:
> - 数据封装到data属性中,用于存储数据统一格式 
> - 封装操作结果到code属性中,用于区分操作 
> - 封装特殊信息到msg属性中,用于表示可选操作  
> 注意: Result类中的字段并不是固定的,可以根据自行需要进行定义

- 定义Code类封装响应码
> 属性分析: 
> - 该类中的响应码都是静态常量 
> - 定义具体数值表示响应成功或失败的具体信息

### 异常处理器
- 核心:基于AOP思想实现的
> 目的: 集中,统一的处理项目中出现的所有的异常.
- 出现异常现象的常见位置
> - 框架内部抛出的异常:使用框架符合规范导致的问题
> > 例如: 在MyBatis,Spring,SpringMVC书写配置文件时,写错了东西,会导致发生异常 
> - ②.数据层抛出的异常: 因为外部服务器故障导致的问题
> > 例如:
> > - 服务器访问超时 
> > - sql语句编写错误,导致异常 
> - 业务层抛出的异常: 因为业务逻辑书写错误
> > 例如: 获取数据是遍历数据
> - 表现层抛出的异常: 因为收集数据,校验数据
> > 例如: 不匹配的数据类型间接导致的异常
> - 工具类抛出的异常: 书写不规范不严谨

- 项目中出现的异常类型(自定义异常)
> - 业务异常(BusinessException)
> - 系统异常(SystemException)
> - 其他异常(Exception)

### 拦截器
- 概述
> 拦截器shiSpringMVC提供的一个强大的功能组件  
> 它可以对用户请求进行拦截,并在请求进入拦截控制器(controller)之前,控制器处理完成之后,响应视图之前去完成相对于的功能增强
- 作用:通过拦截器,可以执行权限验证,日志信息记录
- 拦截器链



## SpringBoot
// SpringBoot是Spring家族中的一个子工程，和Spring框架同属于Spring的产品线
- 作用：优化Spring的使用，如减少配置过程、减少依赖的导入(jar包)
- 优点：
> - 可以独立运行Spring项目
> - 内嵌Servlet容器(内置Tomcat、Jetty服务器)
> - 起步依赖(提供了starter，简化了maven的配置)
> - 自动装配
> - 不需要写xml配置文件且无代码生成

### 配置方式
- 特点：
> - SpringBoot默认使用的是一个全局的配置文件，配置文件的名称是固定的
> - 配置文件必须放在项目的类加载目录下，并且名字必须是application开头
- 配置格式 - 优先级从上到下(properties > yml > yaml)
> - application.properties
> - application.yml
> - application.yaml

### YAML
> YAML(YAML Ain't a Markup Language) - 是一种以数据为中心的标记语言，比XML和properties更适合作为配置文件使用  
> **特点**：以数据为核心，重数据轻格式
- 文件后缀有yaml和yml两种，常用yml，因为优先级较且简短
- 语法规则
> - **大小写敏感**
> - 属性层级关系使用多行描述，每行结尾使用分号结束
> - 使用缩进表示层级关系(py式的强缩进)，缩进时同层级左侧对齐，并且不允许使用Tab键，只允许使用空格
> - **属性值前需要添加空格**，否则不识别
> - 用`#`表示注释
- YAML支持的数据格式
> - 字面量：单个的、不可拆分的值
> - 对象：键值对的组合
> - 数组：一组按次序排列的值
- YAML数据读取的方式是通过配置进行绑定
- 多环境(profile)配置
> 实现情景的切换，开发、测试、生产(实际投入使用)时的不同场景
> - 开发环境(development)
> - 测试环境(testing)
> - 生产环境(production)  
> 一般在application后添加如`-dev`或`-test`样式的后缀即可，但前部分的application不可改变，否则不识别








## SpringCloud
出现的原因：原来的框架存在问题：
- 代码偶合，开发维护困难
- 无法针对不同模块进行针对性优化
- 无法水平扩展
- 单点容错率低，开发能力差
  
垂直拆分优点：
- 系统拆分实现了流量负担，解决了并发问题
- 可以针对不同模块进行优化
- 方便水平扩展，负载均衡，容错率高
> 缺点：系统间互相独立，会有很多重复的开发工作，影响开发效率
  
分布式开发
- 将基础服务进行了抽取，系统间相互调用，提高了代码复用和开发效率
- 系统间耦合度变高，调用关系错综复杂，难以维护

### 注册中心 - 服务注册与服务发现
- 服务注册：服务器将所提供的服务信息(如服务器ip、端口号、访问的协议等)注册/登记到注册中心中
- 服务发现：服务器消费者可以从注册中心获取到实时的服务列表
### 负载均衡
负载均衡就是将用户请求分配到多个服务器的过程，用来提高服务性能
### 熔断机制
### 链路追踪
### 网关


