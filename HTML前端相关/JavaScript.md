# JavaScript
* 主要实现： ECMA + DOM + BOM
## 引入方式
- 内嵌式：可以放在html文件中的任意外置，但是一般放在head里
- 外联式：用src引入，而且不是用link，而是直接用script标签，而且标签内**不写内容**
## 代码小点
### 输出
console.log(输出内容);  控制台输出，其实就是日志
alert(警告框内容);  警告框，实际上这只是简写，全写是window.alert
## ECMA - JS核心基础
### JS基础
#### 特点：
- 变量是弱类型的，类型可以随意变换
- 可以不用声明变量，不声明的时候是**全局变量**
#### 声明变量
- var + 变量名
#### typeof 运算符：类型
- 就是获取变量的类型
- 结果会有：number、boolean、string、object、undefined<br>
  &nbsp;&nbsp;&nbsp;&nbsp; 分别是：数值类型、布尔类型、字符串类型、引用或null类型、没有定义的类型
#### == 与 ===
- == 判断的是内容，而不是类型
- === 判断的是内容和类型
#### 注释
- 与java中的注释一样，单行//，多行/**/
### 数组
#### 创建方式
```text
- var arr = [元素列表];
- var arr = new Array(元素列表);
```
#### 访问元素
- 跟Java中一致，都是中括号跟下标访问
#### 与Java数组对比
- 与Java中的数组不同，JS中的数组长度是可变的，相当于java中的集合
- 可以添加任意类型的数据
#### 属性
- constructor 返回创建对象原型的函数
- length 设置或返回数组中的元素数量
- prototype 允许你向数组中添加属性和方法
### 函数
- 在ECMA中，方法不叫方法，而是叫函数
- 特点：
```text
- 没有访问修饰符，没有返回值类型
- js函数没有重载，重载的会进行覆盖
- 调用时，所给的参数多于定义的参数，不会有影响
    参数给少了就会识别不出，如NAN（不是一个数字）
- 返回值可写可不写
```
```text
- 
```
## BOM 对象 - 浏览器对象模型
* 作用：将浏览器中的各个组成部分封装成对象
### BOM中包含的对象
- window ： 浏览器窗口对象
- Navigator ： 浏览器对象
- History ： 历史记录对象
- Location ： 地址栏对象
- Screen ： 屏幕对象
#### window对象
- 对象中有两种定时器：
```text
- setTimeout(function, time) - 一次性定时器，在所设置的时间之后执行function
- setInterval(function, time) - 循环定时器，循环间隔为所设置的时间，重复执行function
```
#### History 对象
- 上一个页面back()
- 下一个页面forward()
- 去到指定页面go()
#### Location 对象
- 如location.href = "https://网址",页面会跳转到该网址
## DOM 对象 - 文档对象模型
* 作用：将html文档中的各个部分封装成对象
### DOM 中所包含的对象
- Document ：整个文档对象
- Element ：元素对象
- Attribute ：属性对象
- Text ：文本对象
### 常用方法
- getElementById() : 根据id属性值获取，返回单个对象
- getElementsByTagName() : 根据标签名获取，返回对象数组
- getElementsByName() : 根据name属性获取值，返回对象数组
- getElementsByClassName() : 根据class属性获取值，返回对象数组
#### 获取到之后的操作
- 可以更改样式：对属性style中的内容进行赋值，如某个var.style.color = "red"
- 可以更改内容：用法与上同，该属性是innerText
### 事件绑定
- onblur - 元素失去焦点时，通常用于input标签
- onfocus - 元素获取焦点时，通常用于input标签
- onchange - 内容改变时，通常用于select标签
- onsubmit - 当用户点击提交按钮时，用于form标签
- onkeydown - 当用户按下/按住某个按键时，通常于input标签
- onmouseover / onmouseout - 当鼠标经过元素时，用于所有标签
- onclick - 单击某个按钮时，用于所有标签
- onload - 某个页面或者图像被加载完成时，通常用于body标签
- onsubmit - 当用户点击提交按钮时，用于form标签
#### 添加方式
- 方式一：内嵌，直接在标签上加，如onclick = "on()"
- 方式二：内联，也就是使用DOM的获取和赋值，进行绑定，其实就是换了个定义的方法；**注意**：用此方式时，在方法后不能有括号，否则会没反应
- onsubmit - 表单提交事件，返回ture则提交表单，返回false就不会提交表单
## URL 和 URI
- URL包含了URI，如下
```text
例如一个URL：https://www.baidu.com/a/b/src/index.html
其中https是协议，www.baidu.com是套接字和端口号(IP和port)
  剩下的末尾部分是URI，也就是资源地址
```
## 正则表达式
### 创建方式
* 直接写，而且不用加引号：var regex = /正则表达式/;
* 创建RegExp对象：var regex = new RegExp("正则表达式");
### 使用方式
* 正则表达式.test(目标字符串) 会返回一个布尔值
### 字符定义 - 可参照Java中的正则表达式
#### 内容
```text
^ $     - 开始与结束
[]      - 单个字符
\w      - 字母和数字
\d      - 数字
.       - 任意字符
```
#### 数量
```text
+ 至少一个
* 没有或者多个，也就是随意
? 没有或者仅有一个，也就是可以这样记：是个问号，所以想起三目，一般就对应两种结果，true和false，所以0或1
```

- --
# JQuery - JS的轻量级框架
### 使用步骤
* 引入js库
* 添加页面加载函数
### 页面加载函数
#### 使用/创建 方式
- 方式一：$(匿名函数)
- 方式二：全称(匿名函数),也就是jQuery(匿名函数)
#### 特点
- JS页面加载函数只允许有一个，多的会被最后一个覆盖
- 而JQ页面加载函数可以有多个，而且谁在在前，谁先执行
### JS 与 JQuery
* **不能**使用JQ对象调用JS的属性，如$("#div1").innerText = "ttt"; 是改变不了值的
* JS对象也**不能**调用JQ函数
- 所以当我们需要使用时，可以把两个相互转换
#### JS 与 JQuery 的相互转换
- JS -> JQuery (常用)，如：$(div1).text("要写的内容");div1是标签中id
- JQuery -> JS (不常用，因为JQ比JS在实际使用中更简便)，如：$("#div2").get(0).innerText = "要写入的内容";div2是标签中的id
### JQuery 选择器
- 其实很简单，类与id的选择器都差不多，直接上例子吧：
```text
- $("id或类").click(要执行的函数); 可以直接在里面写匿名函数,这个click就是加点击监听
- $("id或类").css("属性名", "属性值"); 属性名如background-color之类的，属性值就比如red、green之类的
- 其实就是写法的的不同，但是本质都是一样的
```
#### 特殊选择器
**可参考css中的特殊选择器，类似用法**
- 选择子类(嵌套的子标签)
- 选择奇偶(仅限同类的序数)：odd(奇数)、even()
- 选择第一或最后
- 属性选择器:
```text
- 如 div[id],这里的id是字面的id，不是标签里设置的id名
```