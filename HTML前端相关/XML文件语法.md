# XML 文件语法
## XML基础
* 文件顶行的<？xml version="1.0" encoding="ISO-8859-1"?>是声明版本和所使用的编码集
#### XML (EXtensible Markup Language)
- XML是一种标记语言，与HTML类似
- XML中没有预定义标签，需要自己定义标签
- XML被设计为具有自我描述性
- XML是W3C的推荐标准
#### XML 与 HTML的区别
- XML被设计用来传输和存储数据
- HTML被设计用来显示数据
- **XML是独立于软件和硬件的信息传输工具**
#### XMl的优势
- 可以经常在不中断应用程序的情况进行扩展，比如在向xml中增加了一个标签，在程序还没有针对新标签更新的情况下，还是能够读到原来的数据的
### 树结构
- 这里所说的树状结构就是标签的嵌套所形成的父子标签的关系，之间只是嵌套关系，并不是我们Java中的父子类关系
- XML中必须要有一个根标签去把所有元素括起来，就类似html中的html标签，但是XML的语法非常严格
### 基本语法
* 在XML中必须都有关闭标签，也就是</标签名>，特别注意的是XML声明，因为声明不是XML元素，所以不需要关闭标签
* XML对**大小写敏感**
* 如上，树结构中的，XML文档必须有一个标签是根标签
* XML的属性值必须加引号，不加引号是错误的(平时有个好的编程习惯可以不用特别注意这个，因为在html中会习惯性的加引号，那么在XML中就不容易出错)
* 在XML中，不能随便写<和&，因为解析器会把它当做新元素的开始，所以需要实体引用来代替，虽然直接写>是合法的，但好习惯是用实体引用来代替<br>
![img.png](pic/XML实体引用.png)
* 注释与html中的注释相同&lt;!--注释内容--&gt;
* 关于空格：html中直接敲的多个空格在显示时会被缩减为一个，但在XML中不会被缩减，敲多少个就是多少个，毕竟XML是数据的存储文件
#### XML元素
- 开始标签到结束标签称为一个元素，或者说从打开标签到关闭标签，注意是包括开始和结束标签的
- 元素内可以有文本和其他元素，也就是嵌套，或者只有文本、或者只有元素(一般如根标签)
#### 命名规则
- 可以含字母、数字以及其他字符
- 不能以数组或者标点开始，也就是说只能字母开头
- 不能以字xml或XML、Xml开头，也就是说选的字母尽量避免与文件的格式类似
- 不能有空格
- **注意大小写敏感**
- 因为没有预定义标签，所以可以使用任何名称且没有保留的字词(保留字词也就是关键字，如java中的this、super等)
* W3C中所提供的最佳命名习惯如下：<br>
![img.png](pic/W3C中XML的最佳命名规范.png)
#### XMl 属性
- 与html中的属性类似，但XML中的属性必须有引号括起来，而如果属性值中有引号，那么需要单双引号进行区别，或者用实体引用来书写引号
- 属性主要是为了程序更放方便地处理数据，属性是不属于数据的组成部分的
- 使用属性会引发的问题：
```text
- 无法包含多重的值(元素可以)，也就是嵌套关系
- 属性无法描述树状结构(同上)
- 属性不易扩展，比如后期修改
- 属性难以阅读和维护，因为XML主要是标签存储数据，那么属性的出现可能会导致内容不够规整，影响阅读

所以不建议用属性描述数据，而应该用标签；属性只是特殊情况做设置的，尽量少用
```
### XML 与 CSS、XSLT
- 在XML 中也可以通过CSS对文档进行样式设置，可以方便阅读和展示
- 但是css并不是XML文件的常用样式设置，W3C更推荐XSLT
#### XSLT的效果
- xml文件内容<br>
![](pic/XML内容.png)
- XSLT样式<br>
![](pic/XSLT样式.png)
- 得出的效果<br>
![](pic/使用XSLT的XML显示结果.png)

## XML HTTP Request 对象
* XML HTTP Request 对象用于在后台与服务器交换数据
* 通过XML HTTP Request对象我们可以做到：
```text
- 在不重新加载页面的情况下更新网页
- 在页面已加载后从服务器请求数据
- 在页面已加载后从服务器接收数据
- 在后台向服务器发送数据
```
* W3C说：所有现代的浏览器都支持 XML HTTP Request对象
### 解析方式
* 创建XML HTTP Request对象 ： xmlhttp = new XMLHTTPRequest();
* IE5和IE6这种老版本创建： xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
#### 读取方式
![](pic/XML文档读取方式案例.png)
## XML 与 JS
## XML进阶
### DTD - 简单约束
* 文件名后缀.dtd
* DTD 的作用是定义XML文档的结构，它使用一系列合法的元素来定义
```dtd
<!ELEMENT students (student*) >
<!ELEMENT student (name,age,sex)>
<!ELEMENT name (#PCDATA)>
<!ELEMENT age (#PCDATA)>
<!ELEMENT sex (#PCDATA)>
<!ATTLIST student number ID #REQUIRED>
```
如上：
- ELEMENT表示元素，后接根标签，括号内规定了子标签；而*是表示0或多个
- 第二行是规定标签内要包含什么子标签，并且要**按规定的顺序**出现，不带符号，表示只能出现一次，类似正则
- name、age和sex括号内规定了只能写字符串
- 最后一行是约束属性，如上约束了student的number属性，只能写类型为ID的数，ID表示这个值是唯一的
- #REQUIRED表示这个是一定要写的
#### 引入方式
### Schema - 复杂约束
* 文件名后缀是.xsd
* 直接看例子去理解写法，如下：(特殊原因，使用的是dtd文本形式显示)
```dtd
<?xml version="1.0"?>
<xsd:schema xmlns="http://www.zzxx.vip/xml"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema"
        targetNamespace="http://www.zzxx.vip/xml" elementFormDefault="qualified">
    <xsd:element name="students" type="studentsType"/>
<!--    -->
    <xsd:complexType name="studentsType">
        <xsd:sequence>
            <xsd:element name="student" type="studentType" minOccurs="0" maxOccurs="unbounded"/>
            <!--            表示student元素是什么类型，type等号后那个是自定义类型；
                            minOccurs表示最少出现0次
                            maxOccurs表示最多出现的次数
            -->
        </xsd:sequence>
    </xsd:complexType>
    <xsd:complexType name="studentType"><!--自定义类型studentType的定义-->
        <xsd:sequence>
            <!--sequence表示内容序列-->
            <xsd:element name="name" type="xsd:string"/><!--这个xsd:string是自带的类型，不是自定义的-->
            <xsd:element name="age" type="ageType" /><!--这个ageType是自定义的类型，下面有定义-->
            <xsd:element name="sex" type="sexType" />
        </xsd:sequence>
        <xsd:attribute name="number" type="numberType" use="required"/>
        <!--use表示必须出现的-->
    </xsd:complexType>
    <xsd:simpleType name="sexType">
        <!--simple表示简单类型
             - 可以定义数据类型
         -->
        <xsd:restriction base="xsd:string">
            <xsd:enumeration value="male"/><!--enumeration是枚举-->
            <xsd:enumeration value="female"/>
        </xsd:restriction>
    </xsd:simpleType>
    <xsd:simpleType name="ageType">
        <xsd:restriction base="xsd:integer">
            <xsd:minInclusive value="0"/>
            <xsd:maxInclusive value="256"/>
        </xsd:restriction>
    </xsd:simpleType>
    <xsd:simpleType name="numberType">
        <xsd:restriction base="xsd:string">
            <!--pattern 表示组成格式
                自定义字符串，如下是使用正则表达式
            -->
            <xsd:pattern value="eee_\d{4}"/>
        </xsd:restriction>
    </xsd:simpleType>
</xsd:schema> 
```
* 使用上：一个xml文件可以同时拥有多个约束
```text
<?xml version="1.0" encoding="UTF-8" ?>
<!-- 
	1.填写xml文档的根元素
	2.引入xsi前缀.  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	3.引入xsd文件命名空间.  xsi:schemaLocation="http://www.zzxx.vip/xml  student.xsd"
	4.为每一个xsd约束声明一个前缀,作为标识  xmlns="http://www.zzxx.vip/xml"
 -->
 <students   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			 xmlns:a="http://www.zzxx.vip/xml"
			 xmlns:b="http://www.zzxx.vip/xml"
 		   xsi:schemaLocation=
					 "http://www.zzxx.vip/xml  student.xsd
					  http://www.zzxx.vip/xml2  student2.xsd
					"
		>
		<!--   xml中可以有多个约束，只需要在约束前加命名即可，如 约束a和约束b，使用如下a:student -->
 	<!--number: eoron_\d{4}-->
	<a:student number="eoron_0001">
		<name>张三</name>
		<age>21</age>
		<sex>male</sex>
	</a:student>
	<student number="eoron_0002">
		<name>张三</name>
		<age>21</age>
		<sex>male</sex>
	</student>

```