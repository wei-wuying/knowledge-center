# Json语法
* JavaScript Object Notation(JavaScript 对象表示法)
* 详细可参考[菜鸟教程JSON](https://www.runoob.com/json/json-tutorial.html)
## 基本语法
- 数据在(名称/值)对中
- 数据由逗号 , 分隔
- 使用斜杆来转义 \ 字符
- 大括号 {} 保存对象
- 中括号 [] 保存数组，数组可以包含多个对象