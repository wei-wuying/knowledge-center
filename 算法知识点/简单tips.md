# 简单Tips
- 解题的一些思路方向
#### 二分查找
> 双闭区间和左闭区间
#### 连续的数据
- 如有序数组等  
> - 快慢指针
> - 相向指针
#### 数组毛毛虫
- 用滑动窗口
> - 快慢指针：主循环为快指针，满指针是只有出现满足的情况才移动
#### 螺旋矩阵
- 坚持区间不变问题，顺时针可以选用左闭右开
- 数据是一圈一圈的，所以可以用一个变量来存起始位置，  
  然后循环每次就是一圈，然后奇数的再特殊中心处理
#### 判断是否存在
- 当我们遇到了要**快速判断一个元素是否出现集合里**的时候，就要考虑**哈希法**  
> 但是哈希法也是**牺牲了空间换取了时间**，因为我们要使用额外的数组，set或者是map来存放数据，才能实现快速的查找
- 只会出现固定数据时，可以考虑映射数组解决，如操作只有26个字母的数据，就可以用长度为26的数组
- 我们遇到了要快速判断一个元素是否出现集合里的时候，就要考虑哈希法了

#### 